
// Matrix Class. 
export class Matrix {
  
  // Default constructor.
  constructor() {
    this.grid = [];
  };

  // Overload the forEach() method to take in a callback function.
  forEach(callback) {
    this.grid.forEach((column, x) => {
      column.forEach((value, y) => {
        callback(value, x, y);
      }); 
    });
  };


  // Get the value at a specific row and column.
  get(x, y) {

    // Get the Column at x.
    const column = this.grid[x];

    // If it exists return the value at y.
    if (column) {
      return column[y];
    }
    return undefined;
  };

  // Set the value at a specific row and column.
  set(x, y, value) {

    // If the column at x doesn't exist, create it.
    if(!this.grid[x]) {
      this.grid[x] = [];
    }

    // Actually Set the grid value at x, y.
    this.grid[x][y] = value;
  };

};

window.matrix = new Matrix();


// Vector Class. 2-tuple.
export class Vect2 {
  // Default Constructor.
  constructor( x, y ) {
    this.set( x, y )
  };

  // Setter Function.
  set( x, y ) {
    this.x = x;
    this.y = y;
  };
};


/**
 *  Module Exports Lesson.
 * 
 *  The Entity Class imports the Vect2 Class. However, there are 2 differes it can be 
 *  done-ish.
 * 
 *  If we exported the Vect2 Class as a Default Export, we the import/export statments 
 *  would look like this:
 * 
 *        export default class Vect2 { ... };             // Vect2.js Default Export.
 *                
 *        import Vect2 from './math.js';                  // Entity.js Import.
 * 
 *  But we want to export the Vect2 Class as a Named Export, so the import/export statments
 *  look like this:
 * 
 *        export class Vect2 { ... };                     // Vect2.js Named Export.
 * 
 *        import { Vect2 } from './math.js';              // Entity.js Import.
 * 
 *  The reason we want to export the Vect2 Class as a Named Export is because we want to 
 *  export more than one class from the math.js file. Destructuring exactly what we want 
 *  will tighten the app a bit. Both technically work. This will be evident later.
 * 
 *  Event Loop Lesson. TODO
 * 
 *  https://nodejs.org/en/docs/guides/event-loop-timers-and-nexttick/#setimmediate-vs-settimeout
 * 
 *  https://www.youtube.com/watch?v=8aGhZQkoFbQ
 * 
 *  https://dev.to/nodedoctors/animated-nodejs-event-loop-phases-1mcp
 * 
 */