/**
 *  Mario Timing.
 *
 *  Old and Busted. - requestAnimationFrame() from the Web API was good. The way to think
 *  about it is that it tells the Browser to "do some logic" before drawing the next
 *  frame.
 *
 *  The browser is in charge of choosing the best moment to execute the code,
 *  which results in a more efficient use of resources, ect.
 *
 *  Also, it lets the browser choose the best frame interval to have. This allows it to
 *  have a long interval in inactive tabs. Ie. The, User could play a CPU intensive game
 *  or something, then open a new tab or minimize the window, and the game would pause,
 *  leaving resources available for other tasks.
 *
 *  New Hotness. - We dont want this. We want to control the frame rate of the game because
 *  we need control later for things like collision boxes, Mario's movement speed and jump
 *  arc. We need animation at a constant speed.
 *
 *  We work in seconds to make things easier to understand.
 *
 */

class Timer {
  constructor(deltaTime = 1/60) {

    // Initalize accumulatedTime and lastTime.
    let accumulatedTime = 0;
    let lastTime = 0;

    // Our Update function to run the Animation. We pass in 0 to start the game "timer".
    this.updateProxy = (time) => {
      // Calculate change from last update() call. Divide by 1000 to work in seconds.
      accumulatedTime += (time - lastTime) / 1000;

      // Log deltaTime.
      //console.log(deltaTime);
      //console.log(mario.entityPosition);

      while (accumulatedTime > deltaTime) {
        //
        this.update(deltaTime);

        // We want to decrease accumulatedTime each iteration.
        accumulatedTime -= deltaTime;
      }

      /**
       *  Use the Web API to call the update() function again and again. This is our custom
       *  game loop to help us break out the speed control.
       *
       *    - update is simply the Callback Function and will be called by the Web API.
       *
       *    - The 1000/XX is the "frame rate" we want or the delay between calls to the
       *      Callback Function.
       *
       *    - performance.now() is the current "timestamp" that we pass to the Callback
       *      Function.
       *
       */
      //setTimeout(update, 1000 / 30, performance.now());

      // Set lastTime to the current time.
      lastTime = time;

      // Call the Web API with the Instances updateProxy Method.
      requestAnimationFrame(this.updateProxy);
    }
  }

  // 
  enqueue() {

    // Call the Web API with the Instances updateProxy Method.
    requestAnimationFrame(this.updateProxy);
  }
  

  // Function to start the Timer Instance.
  start() {

    // Call the Web API with the Instances updateProxy Method.
    requestAnimationFrame(this.updateProxy);
  }
}

// Export Our Timer Class.
export default Timer;
