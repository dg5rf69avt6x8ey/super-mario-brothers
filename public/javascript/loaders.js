// Import MyPromise Implementation.
import MyPromise from "./PromiseImplementation/MyPromise.js";

// Import the Level Class.
import Level from "./Level.js";

// Import our Background Layer Creator.
import { createBackgroundLayer, createSpriteLayer } from "./layers.js";

// Import our Sprite Loaders.
import { loadBackgroundSprites } from './sprites.js';

/**
 *    Function: loadImage
 *
 *    Params:   url (String) - Location of the Image File to be loaded.
 *
 *    Returns:  Promise - Resolves to an Image Object (HTMLImageElement) once the Image has
 *      been loaded.
 *
 *    This function is used to load an Image File into an Image Object. It returns a Promise
 *    that resolves to the Image Object once it has been loaded.
 *
 *    This is Public Fucntion. Exported. Higher Order Function.
 *
 *  */
function loadImage(url) {

  return (
    // The actual Promise we Return. This Resolves to the Image Object.
    new MyPromise((resolve) => {

      // Create a new Image Object.
      const image = new Image();

      // Add an Event Listener to the Image Object that Resolves the Image Object.
      // The load Event is fired when the Image has been loaded to the Browser and is ready.
      image.addEventListener("load", () => {
        resolve(image); // Resolve the Promise with the Image itself.
      });

      // Set the Image's Source to the URL Passed in.
      image.src = url;
    })
  );
};

// Functuction to draw the Tiles to the Level.
function createTiles(level, backgrounds) {

  // Loop through the Backgrounds.
  backgrounds.forEach ( (background)  => {

    // Draw the Tiles to the Level.
    background.ranges.forEach(([x1, x2, y1, y2]) => {
      for (let x = x1; x < x2; x++) {
        for (let y = y1; y < y2; y++) {
          level.tiles.set(x, y, {
            name: background.tile,
          });
        }
      }
    });
  });
};

/**
 *    Function: loadLevel
 * 
 *    Params:   name (String) - Name of the level we want to load.
 * 
 *    Returns:  JSON Object - Contains the information on how to draw the level.
 *
 *    In this function we use the Fetch API to load a Level File. Fetch returns a Promise that
 *    resolves to a Response Object.
 *
 *    We then use the Response Object's json() method to convert the Response Object to a JSON
 *    Object. The function returns this JSON Object. Im calling it The Level Map.
 *
 *    Refactor: Tile Collision. Here were simply moving things that buld the level from the 
 *    Promise Chain to this function. Zero functionality should change. I should learn testing
 *    for things like this sometime. lol. Although, it might not help here.
 * 
 *  */
function loadLevel(name) {

  // Get the path right.
  //console.log(`./levels/${name}.json`);

  /**
   *  Return a Promise that Resolves to the Level Map. This is kinda cool actually. It needs a 
   *  note. Of course we use Web Dev Simplified's Promise Implementation instead.
   * 
   *  Were using a basic plan Jane Promise here. Nothing Special. However the functions we 
   *  pass to the promise are:
   * 
   *      1) A fetch() from the Fetch API with a then() that contains an inline function that 
   *  returns the Response Object as a JSON. This shoulda been an arrow function. I wanted to 
   *  do a regular one.
   * 
   *      And 2) the loadBackgroundSprites() function.
   * 
   *  The cool part is over and the rest simply Resolves to the Level Map. That is all, thank 
   *  you for attending my TED talk.
   */


  return MyPromise.all([
    // 1)
    fetch(`./levels/${name}.json`).then(

      function (levelToReturn) {
        //console.log("levelToReturn: ", levelToReturn.json());
        return levelToReturn.json();
      }
    ),

    // 2)
    loadBackgroundSprites(),

  ]).then( ([levelSpecification, backgroundSprites]) => {
    
    // Instantiate a new Level Object.
    const level = new Level();

    // Create the Tiles.
    createTiles(level, levelSpecification.backgrounds);

    // console.log() the Level tiles in a nice format.
    //console.table(level.tiles.grid);

    // Create the Background Layer. And push it onto the Compositor.
    const backgroundLayer = createBackgroundLayer( level, backgroundSprites );
    level.compositor.layers.push(backgroundLayer);

    console.log("Entites in Level: ", level.entities); // entities should be empty right now.
    // Create the Sprite Layer. And push that onto the Compositor.
    const spriteLayer = createSpriteLayer(level.entities);
    level.compositor.layers.push(spriteLayer);

    // return the Level Object.
    return level;
  });


}

// Export Our Loader Functions.
export { loadImage, loadLevel };
