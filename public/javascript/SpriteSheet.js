/**
 *    Class: SpriteSheet API - Technically its an Interface. Im gonna call it that because.
 *    
 *    this.image - The Image Object we want to use to draw the SpriteSheet.
 *    this.width - The width of the object on the Image File.
 *    this.height - The height of the object on the Image File.
 * 
 *    this.tiles - Stores the tiles we want to use to draw the SpriteSheet.
 *
 *    define(name, x, y)            - Defines the subset of the Image File we want to use. Non-Tile.
 *    defineTile(name, x, y)        - Defines the subset of the Image File we want to use. Tile.
 *    draw(name, context, x, y)     - Draws the Image to the Context.
 *    drawTile(name, context, x, y) - Draws the Tile to the Context.
 * 
 *    Notes:
 * 
 *    There are two Image Files that we are using to create SpriteSheets to use in the game. The
 *    first is TileSheet.png which is used to draw the Background. Any non-character objects. The
 *    second is Characters.gif which is used to draw the Characters. Mario, Goombas, etc.
 * 
 *    The SpriteSheet Class is used to define the subset of the Image File we want to use. It 
 *    handles all the calculations and standardizes the way to actually draw to the Canvas Context.
 * 
 *    It should be noted that there are calcuations that do magnify the size of the Image when it
 *    is drawn to the Canvas.
 *  
 *    Docs: https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/drawImage
 * 
 * */


export default class SpriteSheet {
  // Constructor for our SpriteSheet Class.
  constructor(image, width, height) {
    this.image = image;
    this.width = width;
    this.height = height;
    this.tiles = new Map(); // Map to hold all the Sprites. Unique name for each Sprite.
  }

  /**
   *    Method: define
   *
   *    Params:   nameOfSprite (String) - name of the type of tile.
   *              x, y (int) - Location of the tile to be drawn. With incorrect dimensions.
   *              width, height (int) - Width and Height of the tile to be drawn.
   *
   *    Returns:  Void
   *
   *    This Method creates a buffer (Canvas) for the defined sprite and adds it to the Map of
   *    all the sprites.
   *
   *    This is Public Method. Exposed with the SpriteSheet Class.
   *
   *  */
  define(spriteName, x, y, width, height) {
    // Create a Buffer Canvas Element.
    const buffer = document.createElement("canvas");

    // Set the Width and Height of the Canvas.
    buffer.width = width;
    buffer.height = height;

    // Draw a subset of the Image onto the Buffer.
    buffer.getContext("2d").drawImage(
      this.image,                             //  Image to draw.
      x, y,                                   //  Source X and Y Location.
      width, height,                          //  Source Width and Height.
      0, 0,                                   //  Destination X and Y Location.
      width, height                           //  Destination Width and Height.
    );

    // Add the Sprite to the Map of Sprites.
    this.tiles.set(spriteName, buffer);
  }

  /**
   *    Method: defineTile
   *
   *    Params:   nameOfSprite (String) - name of the type of tile.
   *              x, y (int) - Location of the tile to be drawn. With incorrect dimensions.
   *
   *    Returns:  Void
   *
   *    In this Method is the same as the define() method except we are defining the Tile at the
   *    correct location on the canvas. It Simply calls the define() method with the correct
   *    dimensions.
   *
   *    This is Public Method. Exposed with the SpriteSheet Class.
   *
   *  */
  defineTile(spriteName, x, y) {
    // Simply call our define() method and multiply the x and y by the width and height of the sprite.
    this.define(spriteName, x * this.width, y * this.height, this.width, this.height );
  }

  /**
   *    Method: draw
   *
   *    Params:   spriteName (String) - name of the type of tile.
   *              context (Object) - The canvas context we want to draw to.
   *              x, y (int) - Location of the tile to be drawn. With correct dimensions.
   *
   *    Returns:  Void
   *
   *    This Method does the actual drawing of the sprites to the canvas as the correct dimensions
   *    of the sprite and at the correct location should be handled already.
   *
   *    Note: 3rd and 4th parameters are the location to draw the tile/sprite.
   *
   *    This is Public Method. Exposed with the SpriteSheet Class.
   *
   *  */
  draw(spriteName, context, x, y) {
    // Get the Correct Buffer to draw from.
    const buffer = this.tiles.get(spriteName);

    // Draw the Image and tell it where on the Canvas Contex.
    context.drawImage(buffer, x, y);
  }

  /**
   *    Method: drawTile
   *
   *    Params:   nameOfSprite (String) - name of the type of Tile.
   *              context (Object) - The Canvas Context we want to draw to.
   *              x, y (int) - Location of the Tile to be drawn.
   *
   *    Returns:  Void
   *
   *    In this Method is the same as the draw() method except we are drawing the tile at the
   *    correct location on the canvas. It Simply calls the draw() method with the correct
   *    dimensions.
   *
   *    This is Public Method. Exposed with the SpriteSheet Class.
   *
   *  */
  drawTile(spriteName, context, x, y) {
    // Simply call our draw() method and multiply the x and y by the width and height of the sprite.
    this.draw(spriteName, context, x * this.width, y * this.height);
  }
};