//TODo: Still needs some commenting work.
// Keyboard Key Constants.
const PRESSED = 1;
const RELEASED = 0;

class KeyboardState {

  // Default Constructor.
  constructor() {

    // Holds the Current State. {key: keyCode, value: PRESSED/RELEASED}
    this.keyStates = new Map();

    // Holds the Callback Functions. {key: keyCode, value: Callback Function}
    this.keyMap = new Map();
  };

  // addMapping Method to add a Key Mapping.
  addMapping( keyCode, callback ) {

    // Simply set the mapping.
    this.keyMap.set( keyCode, callback);
  };

  // handleEvent Method to handle the Event.
  handleEvent( event ) {
    // Desctructure the keyCodefrom the Event.
    const { keyCode } = event;

    // Check to see if we have the keyCode mapped.
    if (!this.keyMap.has(keyCode)) {
      
      // Not in the keyMap. Simply Return.
      return;
    }

    /**
     *  preventDefault() - Web API call that that if the event does not get explicitly 
     *  handled, its default action should not be taken as it normally would be.
     * 
     *  The event contunues to run as normal.
     * 
     *  Source: https://developer.mozilla.org/en-US/docs/Web/API/Event/preventDefault
     */
    event.preventDefault();

    // Get the Keys State.
    const keyState = event.type === "keydown" ? PRESSED : RELEASED;

    // If the state in the Map is the same as the event state we simply want to return.
    // Before we do anything.
    if (this.keyStates.get(keyCode) === keyState) {
      return;
    }

    // Set the state in the map to the event state.
    this.keyStates.set(keyCode, keyState);

    // Log for Debugging.
    //console.log(this.keyStates);

    // Call the appropriate callback for the keyCode AND pass in the keyState.
    this.keyMap.get(keyCode)(keyState);
  };

  // listenTo Method to bind a Event Listener to the Window.
  listenTo( window ) {

    // Here were binding two Event Liteners to the window that gets passed in. One for the
    // keydown Window Event and one for the keyup Window Event.
    ['keydown', 'keyup'].forEach( (eventName) => {
      window.addEventListener( eventName, event => {
        this.handleEvent( event );
      });
    });
  };

};

// Export the KeyboardState Class.
export default KeyboardState;