// Import our Vect2 Class.
import { Vect2 } from './math.js';

// 
class Trait {

  // Default Constructor.
  constructor( name ) {
    this.NAME = name;
  };

  // Update Method. Does nothing by default except throw an error if it isnt overridden.
  update() {
    console.log( 'Unhandled update call in Trait.' );
  };

};

// Export our Trait Class.
export { Trait };


// Entity Class.
export default class Entity {

  // Default Constructor.
  constructor() {

    // Position and Velocity Info for the Entity.
    this.entityPosition = new Vect2(0, 0);
    this.entityVelocity = new Vect2(0, 0);

    // Size Dimensions for the Entity.
    this.entitySize = new Vect2(0, 0);

    // Place to store all the Traits.
    this.traits = [];
  };

  // Meethod to add a Trait to our Traits Array.
  addTrait( trait ) {
    
    // Push the Trait onto the Array.
    this.traits.push( trait );

    // This line is kinda importatnt. This is wahat allows us to call the entities trait
    // as a Method by name. For example, mario.jump.start() or mario.jump.cancel() to 
    // start or stop Mario's jump.
    this[trait.NAME] = trait;
  }


  // Method to update the Entity with the deltaTime.
  update( deltaTime ) {

    // For each Trait in our Array we want to update it with deltaTime.
    this.traits.forEach( (trait) => {
      trait.update( this, deltaTime );
    });
  };
};
