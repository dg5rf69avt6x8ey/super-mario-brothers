// Import SpriteSheet Class.
import SpriteSheet from "./SpriteSheet.js";

// Import our Loaders Helper File.
import { loadImage } from "./loaders.js";


/**
 *  General Notes:
 *    These Helper Functions are use to load the Sprites for both the Background and Mario
 *    from the actual Image Files. We load the Image for the Sprite then create a new SpriteSheet 
 *    Object and define the Tile locations before we returnthe SpriteSheet Objects which is used 
 *    elsewhere to do things.
 * 
 */


/**
 *    Function: loadBackgroundSprites
 * 
 *    Params:  None
 * 
 *    Returns:  backgroundSprites (SpriteSheet) - The SpriteSheet we want to use to draw the 
 *                Backgrounds.
 *
 *    This function uses a hardcoded path to the Background Image File, TileSheet.png, and
 *      loads the images from the defined locations on the Image File to a SpriteSheet Object.
 * 
 *    This is Public Fucntion. Exported. Higher Order Function.
 *
 * */
function loadBackgroundSprites() {

    return loadImage("./images/TileSheet.png").then((image) => {
    //console.log("Image Loaded.", image);

    // Create a SpriteSheet Object.
    const sprites = new SpriteSheet(image, 16, 16);

    // Define the name and location of the sprites we want to use.
    sprites.defineTile("ground", 0, 0); // 2nd and 3rd parameters === location of sprite.
    sprites.defineTile("sky", 10, 7);

    // Test draw the Sprites to the Canvas. I like this. Im keeping it. Not nesesary.
    //sprites.draw('sky', context, 32, 32); // 2nd and 3rd parameters === location of sprite.

    // Actually Return the Sprites. This is whats getting returned because of the then() call.
    return sprites;
  });
}

/**
 *    Function: loadMarioSprite
 * 
 *    Params:  None
 * 
 *    Returns:  marioSprite (SpriteSheet) - The SpriteSheet we want to use to draw Mario and 
 *                various positions.
 *
 *    This function uses a hardcoded path to the Character Image File, Characters.gif, and
 *      loads the images from the defined locations on the Image File to a SpriteSheet Object.
 *      This one is Mario Specific.
 * 
 *    This is Public Fucntion. Exported. Higher Order Function.
 *
 * */
function loadMarioSprite() {
  return loadImage("./images/Characters.gif").then((image) => {

    // Create a SpriteSheet Object.
    const sprites = new SpriteSheet(image, 16, 16);
    sprites.define("idle", 276, 44, 16, 16);                // Define Position for this Sprite.

    // Actually Return the Sprites. This is whats getting returned because of the then() call.
    return sprites;
  });
}

// Export Our Sprite Loader Functions.
export { loadBackgroundSprites, loadMarioSprite };
