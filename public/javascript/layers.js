/**
 *    Function: drawBackground
 * 
 *    Params:   background (Object) - Stores the information on how to draw the Background.
 *              context (Object) - The Canvas Context we want to draw to.
 *              sprites (SpriteSheet) - The SpriteSheet we want to use to draw the Background.
 * 
 *    Returns:  Void.
 *
 *    This function uses a single background Object from all the backgrounds in our Level Map
 *    to draw the background to the Canvas Context using the given SpriteSheet.
 *
 *    The function is called for each background property in levelMap.backgrounds (ie. ground,
 *    sky, ...etc.) and simply iterates through the ranges, drawing each tile from the SpriteSheet
 *    to the correct location on the Canvas Context.
 * 
 *    This is Private Function. Not Exported.
 * 
 *    Depricated: We dont need this anymore because we are drawing the Background Layer from the
 *    Level Map now.
 *
 * */
/*function drawBackground(background, context, sprites) {

  // Log for Debugging.
  //console.log({ background, context, sprites });
  
  // Draw the Background. TODO: Change the Array to an Object and Destructure the Object. EC4.
  background.ranges.forEach(([x1, x2, y1, y2]) => {

    //console.log({x1, x2, y1, y2});
    for (let x = x1; x < x2; x++) {
      for (let y = y1; y < y2; y++) {
        sprites.drawTile(background.tile, context, x, y);               // Actually draw the tile.
      }
    }
  });
};*/

/**
 *    Function: createBackgroundLayer
 * 
 *    Params:   backgrounds (Array) - Stores the information on how to draw the Backgrounds.
 *              sprites (SpriteSheet) - The SpriteSheet we want to use to draw the Backgrounds.
 * 
 *    Returns:  Function - Draws the background buffer to the Canvas Context.
 *
 *    This function uses the backgrounds Array from the Level Map and the Background Sprites to
 *    create a background buffer then draw the tiles for each of the backgrounds to the buffer
 *    using the given SpriteSheet.
 * 
 *    This is Public Fucntion. Exported. Higher Order Function.
 *
 * */
function createBackgroundLayer(level, sprites) {
  // Create a seperate Background Buffer and set its dimensions.
  const buffer = document.createElement("canvas");
  buffer.width = 256; // This should be 256 to keep Nintendo dimensions.
  buffer.height = 240;  // I think this is the same.

  // Create a seperate Context for the Background Buffer.
  const context = buffer.getContext("2d");

  // Draw the Tiles from our Level Map source of truth. Old and Busted.
  /*level.tiles.grid.forEach(( column, x ) => {
    column.forEach(( tile, y ) => {
      sprites.drawTile(tile.name, context, x, y);
    });
  });*/

  // Draw the Tiles from our Level Map source of truth. New Hotness.
  level.tiles.forEach(( tile, x, y ) => {
    sprites.drawTile(tile.name, context, x, y);
  });

  // Return a function that draws the Background Buffer to the context.
  return function drawBackgroundLayer(context) {
    context.drawImage(buffer, 0, 0);
  };
}

// Higher Order Function to draw the Entities Sprite Layers to the Canvas Context.
function createSpriteLayer( entities ) {

  // Returns our old sprite.draw() function. Exactly the same.
  return function drawSpriteLayer( context ) {
    entities.forEach( (entity) => {
      entity.draw( context ); // Draw the Sprite to the Context w/ the Position.
    });
  };
};

// ** Theres some hijinx going on here. **
// Higher Order Function to draw the Collision Layer to the Canvas Context. 
function createCollisionLayer( level ) {

  // Place to hold our Resolved Tiles that the Entity is colliding with.
  const resolvedTiles = [];

  // Get the TileResolver from the Level's tileCollider. 
  // tiles is of Type TileResolver after all.. 
  const tileResolver = level.tileCollider.tiles;

  // Get the Tile Size.
  const tileSize = tileResolver.tileSize;

  // Save the getByIndex Method from the tileMatrix. We need it for later.
  const getByIndexOriginal = tileResolver.getByIndex;

  // Override the old getByIndex Method in the tileResolver.
  tileResolver.getByIndex = function getByIndexFake(x, y) {

    // Console Log for Debugging.
    //console.log({x, y});

    // Push the coordinates of the Resolved Tile to the Array.
    resolvedTiles.push({x, y}); // Push to an Object.

    // TODO:
    return getByIndexOriginal.call(tileResolver, x, y);
  };

  /**
   *  Were going to return a function that draws a red box around the tiles that
   *  the entitiy is colliding with.
   * 
   *  We simply draw a red path for each of the {x, y} coordinates that we get
   *  from the tileMatrix.
   * 
   */
  return function drawCollisionLayer( context ) {
    context.strokeStyle = "blue";
    
    resolvedTiles.forEach(( {x, y} ) => {
      //console.log("Would Draw: ", x, y);
      context.beginPath();
      context.rect(x * tileSize, y * tileSize, tileSize, tileSize);
      context.stroke();
    });

    level.entities.forEach( (entity) => {
      context.strokeStyle = "red";
      context.beginPath();
      context.rect(entity.entityPosition.x, entity.entityPosition.y, entity.entitySize.x, entity.entitySize.y);
      context.stroke();
    })
    // Reset the resolvedTiles Array.
    resolvedTiles.length = 0;
    };
};






// Export the Background Layer Creator Function.
export { createBackgroundLayer, createSpriteLayer, createCollisionLayer };