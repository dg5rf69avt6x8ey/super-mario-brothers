Hello, this is my first commit. Second commit.

Super Mario Brothers - Meth Meth Method.
https://www.youtube.com/watch?v=g-FpDQ8Eqw8&list=PLS8HfBXv9ZWWe8zXrViYbIM2Hhylx8DZx

Promise Implementation - Web Dev Simplified
https://www.youtube.com/watch?v=1l4wHWQCCIc

# Links and things.
GitLab Repository: https://gitlab.com/dg5rf69avt6x8ey/super-mario-brothers/

Amazon S3 Bucket Website Endpoint:
http://k35wj4a7ldb4n5-smb.s3-website-us-east-1.amazonaws.com/



# Notes -  Episode 1: https://www.youtube.com/watch?v=g-FpDQ8Eqw8
- He uses Serve to serve the files locally. Were just going to use the Live Server Extension for VS Code. Dev thing only.

# Notes -  Episode 2: https://www.youtube.com/watch?v=FF93S8rLL_Q
- Refactor Loading Sprites and Levels. - Previously, the loadLevel() function was highly coupled to the loadImage() function. This isnt a huge problem if they were normal functions, but they are async functions. What we want to do is wrap the loadLevel() function AND the loadImage() function in a Promise so that we wait for both to finish before we do anything else. Then we get the resolution from the Promise and then we do things to the results.
- Moving Mario. Create Background Buffer. - To animate Mario we use the Window.requestAnimationFrame() from the Web API. requestAnimationFrame() will work better with all browsers render cycle than setInterval() or setTimeout(). Also we made some changes to the way the background is drawn. We draw the backround once to the backgroundBuffer and then we draw the backgroundBuffer to the screen. This way we only have to redraw the backgroundBuffer once.
-Add Compositor. Refactoring. - Theres some refactoring here. layers.js is new and a function is moved there. Compositor.js is added. Compositor.js is added.

# Notes -  Episode 3: https://www.youtube.com/watch?v=HlloFDayGgk
- Add Vector Class. Add Entity Class. - We add Velocity and Gravity to help move Mario. This will help us simulate jumps and things later. We create the Vector Class. We round this out by refactoring the .then() of our Promise Chain by creating the Entity Class. set() method added.
-  Add drawMario() and updateMario(). - We added the drawMario() and updateMario() functions. Also, we moved the Vect2 abd Entity Classes.
- Create entities.js. Refactoring - We simply moved Mario and refactored a bit.
- Timing Refactor. - **Currently, the only "timing" mechanisim in the game right now is the requestAnimationFrame() call we make to the Web API.** 1) The problem with this is Mario needs some sort of timing mechanisim that is independent of some optimized process like requestAnimationFrame() because we need to do things like colisions with Baddies. And 2) we also want more control over the speed of Mario because this will change. We break that out here. We start by removing requestAnimationFrame() and using setTimeout() and the Web APIs per formance.now(). SetTimeout() controls the refresh rate. We also update our update() Function. deltaTime by the end here should all be the same and Mario is adjusted to do a normal parabola. deltaTime is calculated.
- Timeing Refactor 2. - There is another problem with the timing. Every refresh Mario travels in a different parabola. This is a problem because we need to have Mario be constent in his movement. To fix this we make deltaTime a constant but our old problem of controling his speed despite frame rate is back. To solve this we calculate the change since the last frame and update only if its greater than the interval we want (deltaTime). We now decopleed the rendering frame rate from the internal game timing mechanism.
- Timeing Refactor 3. - We simply refactor to the Timer.js Class.

- Fix from 4. -  **A few Fixes that are in Episode 4 but should be in Episode 3.** 1) I fix a minor logging thing in layers.js. Nothingburger. 2) We update gravity so Marios Velocty is gravity * deltaTime to better reflect how it really should be irl. 3) We moved draw to below update in our timer.update Instance because obviously we want to update then draw.

# Notes -  Episode 4: https://www.youtube.com/watch?v=1rBOUyRGQuU
- Build KeyboardState Class. - We simply build out the KeyboardState Class. We add a mapping and hook it up to the window object by listening to it. The end result here is that we can hit the spacebar and it will log if its a keydown or keyup event.
- Small Fix. - Just a small fix to the KeyboardState Class. Return nothing instead of false.
- Build Trait Class. - We want to use the keyboard to control Mario for things like running and jumping. Todo this we need to further build out the Entity Class (which holds our position data) and the KeyboardState Class to work together. This is why we build out the Trait Class. And as a consequence of buiding out the Trait Class we build out our first trait, the Velocity Class. We get an error at the end of this commit because we havent added the Jump Trait to Mario yet. Marios parabola should work.
- Build Jump Class. - We added a new Jump Class to the Traits folder. We added it to the Mario Entity which was easy because of the previous commit.

# Notes -  Episosde 5: https://www.youtube.com/watch?v=YLMP5jmtpYc
- Refactoring. Add Level Class. - We start by doing some refactoring to prepare the game for Tile Collisions. To start we get move a bunch of stuff our of our main.js related to the building of the level. We want to keep that all together so we build out the Level Class. As a result of this we need to refactor our level loaders in loaders.js. The juice is in the loaders.js but its pretty small. The big thing out of this is it ties all the Entities to the level and it all gets updated from the same spot, Level.update(deltaTime). Also, it sets us up for Tile Collisions. 
- Build out Level Tiles. - This we created a new function, createTiles(), by simply copying drawBackground() and modified it to draw the tiles. We added the Tiles to the Level Class. Tiles in the Level Class will be the gospel in our game.
- Level/Background Refactor. - We want the tiles in the Level Class to be the single source of truth for our game to do this we need to do some refactoring on how we create the Background Layer. So we added a Level Object as a parameter and needed to make a bunch of changes as a consequence. JUICE, We also overload the forEach Method in the Matrix Class and pass in a Callback to make drawing Tiles nicer. **The whole point of this was to set up something that would share the logic of drawing the background and the collision tiles.**
- Build T. Collider/Resolver Class. - We start the Tile Collision part by creating a Tile Collider Class and adding it to our Level Class. We also tie it our entities in the update() Method. We create a Tile Resolver Class and move it to its own file. 
- Small Fixes. - Small changes not worth mentioning. Mostly logging.
- Debugging Things. - We added the ability to move Mario using the UI Events API. Also, added some level configuration. I also fucked up the Level file location and fixed that this commit. Amazing its gotten this far. Also he had an extra block overrideing in the level JSON file. I fixed that. Also, started to keep track of the objects in the levels json folder so I could track it better myself in the readme.md. Its gonna change.
- Commenting and JSON Stuff. - This was a long overdue build and I think it was in the right spot. So what did we do, 1) First, I updated the 1-1.json to include more ground Tiles as per the video. Cool. Also added some reference ground Tiles to help the visuals. And some comments to the JSON File. This will change next. I know it. Not really. 2) I added a big'ole commenting section on loading the Level. Mentioned. And a no news comment remove. maybe an add. I hope the readme file doesnt look like trash with the comments.
Im leaving the next line in celebration of the commenting.
- Y-Axis Collision Detection. - We build out the checkY Method in the Tile Colider.js. ENDS -> 44:04
- Create the Collision Layer. - We create the createCollisionLayer function in layers.js and add it to our main.js. In the video he doesnt add it to the context. We need to do that. Lesson: This kinda should have been painfully obivous to me by now. We also modify the Jump Trait in the Jump.js file (51 minutes). Theres a good debugging lesson here because the trait order matters in the update() Method. Collision in the Y-Axis is working. Mario now doesnt fall through or jump through the ground.
- Entity Size and Size Box. - We started by adding size to the Entity Class. We then draw a box in the Collision Layer to show the size done by simply looping over each entity in the level file and addin a box.
- Finish Collision Box. - Started by making the getByIndex and seachByIndex Methods in the TileResolver. Renamed matchedByPosition to searchByPosition Also, searchByRange gets added where we can search for matched tiles by a range of x and y positions. Updates to the TileCollider Class to reflect these changes. Last, Mario is actually colliding with the ground with his head so we "bump" up his collison box by a Tile in the TileCollider.
- Add Go Class. - We add the Go Trait so that mario can move. This is it. Well we make the nesseary fixes also. Added Keybinds for A and D. Moved SPACE to W key. Very rudamenty.
- X-Axis Collision Detection. - Same thing as Y-Axis pretty much. Just minor modifications to the Method. We also update entities position from the Level Object. This was kinda a Heavy move. I still have a velocity bug. MARIO WONT STOP!!!!!!!! Which I hope gets resolved next commit. maybe. Also added a optimization to do collision checking for jsut outside the collision box and not E.V.E.R.Y cell in the collison box.
- Fixes. - Nothing worth reporting.
- Keyboard Refactor. - Smallish refactor of the Keyboard input. Clean up main.js mostly. Gravity gets put on the Level Class.

# TODO: (3/6)
- DONE. Figure out where to actually deploy it. Probuably just Amazon S3.
- SpriteSheet Class Commenting. Whole thing sucks. SpriteSheet.js 
- DONE loadImage(), loadLevel() Commenting. loader.js
- DONE drawBackground() Commenting. main.js
- POSSIBLE - Right now the background sprites (the 'ground' and 'sky'. Those things.) is dependent on the SpriteSheet.define() method setting the sprite AND it must be configured properly in the level.json file. I want to decouple this if possible. See lines 54-56 in main.js.
- sprite.js File Commenting. Whole thing sucks. sprite.js
- This has been depricated. Too much work right now. Might bring it back.

# Commenting Tracker
main.js - Main is pretty well commented. Im fine with it. It will change a lot too.
layers.js - 2/2 - createBackgroundLayer(), drawBackground()
loader.js - 2/2 - loadImage(), loadLevel()
sprites.js - 2/2 - loadBackgroundSprites(), loadMarioSprite()

Class Files:
math.js - 0/1 - Vect2 Class, Module Exports Lesson
SpriteSheet.js - 0/1 - SpriteSheet Class
Entity.js - 0/1 - Entity Class
Compositor.js - 1/1 - Compositor Class
PromiseImplementation.js - 0/1 - PromiseImplementation()
- This has been depricated. Too much work right now. Might bring it back. Commenting does needs to be finalized at some point.

# Extra Credit:
1) Some sort of in-game cheat code. (ex. Pressing the Up Arrow Key 5 times, or something like that.)
2) Parse readme.md and display it on the S3 Endpoint.
3) DONE. Web Dev Simplified Promise Implementation. https://www.youtube.com/watch?v=1l4wHWQCCIc TODO: Commenting.
4) I made changes to the format of the Level file format. (ex. levels/1-1.json, The Level Tile Definititons.). Those got backed out. They should go back in because they were better. Hopefully, it should make it easier to read the json files. I have a feeling its going to be a headache tho.

# Javascript Lessons
All in math.js right now. Go there for them.


** Remember to push files to S3 Also!!! **

# Git Stuff. Source Control.
git init --initial-branch=master

git config --global user.name "dg5rf69avt6x8ey"

git config --global user.email "dg5rf69avt6x8ey@gmail.com"

git remote add origin https://dg5rf69avt6x8ey@gitlab.com/dg5rf69avt6x8ey/super-mario-brothers.gits

git add .

git commit -m "Keyboard Refactor."

git push -u origin master




Levels Data Format. 
\super-mario-brothers\public\levels\1-1.json

{
  "backgrounds": [
    {
      "tile": String,
      "ranges": [
        [ 
          "xStart": Number, "xEnd": Number, 
          "yStart": Number, "yEnd": Number
        ],
      ],
    },
  ]
}


** NEEEEEEEEEDS TO BE FIXED **


/**
 *  Loading the Level. A Guide.
 *  
 *  1) We start by using the Fetch API to load the Level from the JSON File located 
 *  at `./levels/${name}.json`. This contains the information we define to actually
 *  build the level and draw the background objects. It returns a JSON Object called
 *  the `levelSpecification`. 
 *  
 *  Additionally, we load the Background Sprites from `./images/TileSheet.png` into
 *  a new SpriteSheet Object called `backgroundSprites`. 
 *  
 *  The SpriteSheet Object we return contains 1) the Image of all the Sprites we 
 *  want (the actual drawings), 2) a Map that matches individual Sprites to a Tile 
 *  Index position on the Image and 3) a Size that defines the width and height of 
 *  all the Sprites.
 *  
 *      Note: Each Tile Index position is a (x, y) touple that represents a specific 
 *      Background Sprite on the TileSheet.png. Sometimes, they are complete objects 
 *      like a brick, coin, sky tile and sometimes they are one 16x16 segment of a 
 *      larger complete object. They all are 16x16 pixels in size. Well, we can 
 *      specify any size we want but we are using 16x16 for this TileSheet.png.
 *  
 *      We then use the Tile Index to multiply it by the size (16 x 16) to get the  
 *      actual positions of the Image. We then save each of them to their own Buffer 
 *      and store them on a map to do things later. 
 *  
 *      Note: The size is important because it allows us to grab the entire Sprite 
 *      from the PNG File by simply giving it an Index. We dont have to specify the 
 *      pixel locations. In order to get the actual pixel locations we then multiply 
 *      the Index by the size to get the location of the correct Sprite to be 
 *      rendered or whatever later.
 *  
 *      TODO: More on loadBackgroundSprites() - sprites.js. Theres some below but 
 *      its not complete.
 *  
 *  
 *  2) Once we have the `levelSpecification` and the `backgroundSprites` we can build
 *  the Level.
 *  
 *  We start by creating a new Level Object and adding Tiles to the `Level.tiles` 
 *  Matrix. Its a simple 2D Array of Jacascript Objects that has, as of right now, 
 *  only a String called `name` in it. This is the name of the Tile that it is.
 *  
 *  The Tile Matrix is our main source of truth. And we fill it out by simply 
 *  lopping though all of the backgrounds that we get from the `levelSpecification`. 
 *  This in effect will be our main gameboard.
 *  
 *  3) After We've populated our Tiles, we need to create a new Background Layers by
 *  combining the new Level Object and the `backgroundSprites`. 
 *  
 *  This is done by looping thought the `Level.tiles` Map we just created and 
 *  drawing the Tiles eventually this gets pushed onto the Compositor on the new 
 *  Level Object. We do some Javascript hijinx here that I should understand but I 
 *  dont want to go over it agian right now. 
 *  
 *  4) We do the same thing with the Entity Sprites. Which hasent been done yet. 
 *  
 *    TODO:
 *  
 *  
 *  5) We return the Level Object. The Level Object is now complete. We continue to 
 *  main.js.
 *  
 *  
 *  
 *  
 *  Updating and Drawing the Level. A Guide.
 *  
 *    TODO:
 *  
 *  
 *  Adding Entities to the Level. A Guide. 
 *  
 *    TODO:
 *   
 *  Tile Collision Primer. May need a mini Matched Tile Primer here.
 *  
 *    TODO:
 *  
 *  
 *  The Files: to load the level we are using 2 different Files.
 *  
 *    levels/1-1.json
 *    1) The Level File. This is a JSON File.
 *  
 *      Note: Order matters for the backgrounds array of Objects. The top one gets
 *      overlapped by the ones below. And thus, the bottom one is the only one that
 *      doesnt get overlapped. This is I think because the Compositor store is just 
 *      an Array and the order of the Array is the order of the Layers. 
 *  
 *    So, We need some sort of way to define wahts contained in the levels for the 
 *    Player Entity (Mario) to interact with. To do this we use a Level File and in 
 *    it contains everything we need to know to build the level. We are actually
 *    defining the locations of obsticals and backgrounds and things like blocks, 
 *    coins, pipes, clouds, sky and ground are.
 *  
 *    Inside the JSON Object we have one property right now called `backgrounds`.
 *    This is an Array of Objects and each Object contains a `title` and a `range` 
 *    property at the moment.
 *  
 *    The `title` property is a String that defines the type of background object we
 *    want to draw. Its just a unique identifier of that particular Sprite.
 *    
 *    The `range` property is an Array of 4 Numbers that define the locations in the 
 *    level we want to draw that Sprite/Tile. This property needs a bit of  
 *    explination.
 *  
 *    Each of the first two Numbers are the starting Index and ending Index for the 
 *    X-coordinates of whatever Sprite/Tile you are defining. Respectively. The 
 *    second two are the starting spot and ending spot for the Y-coordinates. 
 *  
 *      TODO: I want to change this Array to an Object eventually so that it comes in
 *      better. More defined. I want the names to mean something.
 *  
 *  
 *    The Level Index starts in the upper left corner of the level at 0, 0 and go to 
 *    the bottom right corner of the level at 64, 64. In reality it goes past that as
 *    far as you want the level to be. Im pretty sure this is like because the way we
 *    are filling out the Matrix. Ya it is, We fill out the array from top to bottom,
 *    left to right. 
 *  
 *      Note: Index/Tile vs Pixel Locations: Through out Mario we use Index 
 *      Locations and Pixel Locations. They arnt interchangeable.
 *  
 *      We can define the size to however we want (probuably, need since it depends 
 *      on the TileSheet.png used) so we hardcode it into the instinantiation of the 
 *      SpriteSheet Class.
 *  
 *      One thing we can do is a lot of converting between Index/Tile number and 
 *      pixel locations. This is mostly done on load of the TileSheet.png and the 
 *      rendering of the Sprites. TODO: More talk on this.
 *  
 *    
 *    2) The Background Sprite File. 
 *  
 *    This is a PNG File that contains the individual Sprites for all of the 
 *    Background Objects. It consists of the acutal Images we want to draw to the 
 *    Canvas Context and render to the User (different from the player but same 
 *    thing, this is because player is the entitiy in the game user is the viewer).
 *    The one we use is a 
 *  
 *    We actually hardcode the Indexes of the Sprites in the loadBackgroundSprites()
 *    function of the sprites.js File. Once we have instantiated the SpriteSheet 
 *    Object with a defined size. It should be noted that this is the Index of the 
 *    Sprites on the TileSheet.
 *  
 */